<?php

include ('routeLogin.php');
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->group('/auth', function() use ($app){

    $app->get('/logout', function (Request $request, Response $response){
        session_destroy();
        return $response->withHeader('Location', $this->router->pathFor('login'));
    })->setName('logout');
$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');

$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');

$app->get('/osoby', function (Request $request, Response $response, $args) {
    $q = $request->getQueryParam('q');
    try {
        if (empty($q)) {
            $stmt = $this->db->prepare(
                ' SELECT person.*, location.*, pocty.cnt AS pocet_schuzek, halo.cnt AS pocet_kontaktu 
                        FROM person
                        LEFT JOIN location USING (id_location)
                        LEFT JOIN (SELECT id_person, COUNT (*) AS cnt
                        FROM person_meeting 
                        GROUP BY id_person)
                        AS pocty
                        USING (id_person)
                        LEFT JOIN (SELECT id_person, COUNT (*) AS cnt
                        FROM contact
                        GROUP BY id_person)
                        AS halo
                        USING (id_person)
            ORDER BY last_name');
        } else {
            $stmt = $this->db->prepare(
                'SELECT person.*, location.*, pocty.cnt AS pocet_schuzek, halo.cnt AS pocet_kontaktu 
                        FROM person
                        LEFT JOIN location USING (id_location)
                        LEFT JOIN (SELECT id_person, COUNT (*) AS cnt
                        FROM person_meeting 
                        GROUP BY id_person)
                        AS pocty
                        USING (id_person)
                        LEFT JOIN (SELECT id_person, COUNT (*) AS cnt
                        FROM contact
                        GROUP BY id_person)
                        AS halo
                        USING (id_person)
        WHERE last_name ILIKE :q
            ORDER BY last_name');
            $stmt->bindValue(':q', $q . '%');
        }
        $stmt->execute();
    } catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());

    }
    $tplvars['osoby'] = $stmt->fetchAll();
    $tplVars['q'] = $q;

    return $this->view->render($response, "osoby.latte", $tplvars);
})->setName("osoby");

/*app->get('/pridej', function (Request $request, Response $response, $args) {
  $tplVars['form'] = ['ln' => '', 'fn' => '', 'nn' => ''];
return $this->view->render($response, 'formular.latte', $tplVars);
})->setName('formular');


$app->post('/pridej', function (Request $request, Response $response, $args) {
  $data = $request->getParsedBody(); // v podstat2 $_POST
    if (!empty($data['fn']) &&
      !empty($data['nn']) &&
        !empty($data['ln'])) {

        try {
           $stmt = $this->db->prepare('INSERT INTO person(
last_name, first_name, nickname, gender, height, birthday)
VALUES(:fn, :ln, :nn, :g), :h :b');
           $g = empty($data['g']) ? null : $data ['g'];
         $stmt->bindValue(':g', $g);
           $stmt->bindValue(':fn', $data['fn']);
            $stmt->bindValue(':ln', $data['ln']);
            $stmt->bindValue(':nn', $data['nn']);
            $stmt->bindValue(':h', $data['h']);
           $stmt->bindValue(':b', $data['b']);
           $stmt->execute();
        } catch (Exception $ex) {
            if ($ex->getCode() == 23505) {
                $tplVars['form'] = $data;
                $tplVars['error'] = 'Tato osoba uz existuje.';
                return $this->view->render($response, 'formular.latte', $tplVars);
            } else {
                $this->logger->error($ex->getMessage());
                die($ex->getMessage());
            }

        }
        return $response->withHeader(
            'Location',
            $this->router->pathFor('osoby'));
    } else {


  $tplVars['form'] = $data;
        return $this->view->render($response, 'formular.latte', $tplVars);
    }
});*/
$app->post('/delete-person',function(Request $request, Response $response, $args){
$id=$request->getQueryParam('id');
try{
    $stmt=$this->db->prepare('DELETE FROM person WHERE id_person= :id');
    $stmt->bindValue(':id', $id);
    $stmt->execute();

}catch (Exception $ex){
    $this->logger->error($ex->getMessage());
    die($ex->getMessage());
}
return $response->withHeader('Location', $this->router->pathFor('osoby'));
})->setName('deletePerson');

$app->get('/edit-person',function(Request $request, Response $response, $args){
    $id=$request->getQueryParam('id');
    try{
        $stmt=$this->db->prepare('SELECT * FROM person WHERE id_person= :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();

    }catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die($ex->getMessage());
    }
    $person= $stmt->fetch();

    $tplvars['form']=[
        'ln'=>$person['last_name'],
        'fn'=>$person['first_name'],
        'nn'=>$person['nickname'],
        'h'=>$person['height'],
        'g'=>$person['gender'],
        'b'=>$person['birth_day'],
    ];
    $tplvars['id']=$id;
    return $this->view->render($response,'edit-person.latte', $tplvars);
})->setName('editPerson');

$app->post('/edit-person',function(Request $request, Response $response, $args){
    $id=$request->getQueryParam('id');
    $data=$request->getParsedBody();
    try {
        $stmt = $this->db->prepare('UPDATE person SET
last_name=:ln, first_name=:fn, nickname=:nn, gender=:g, height=:h, birth_day=:b');
        $stmt->bindValue(':g', empty($data['g']) ? null : $data ['g']);
        $stmt->bindValue(':fn', $data['fn']);
        $stmt->bindValue(':ln', $data['ln']);
        $stmt->bindValue(':nn', $data['nn']);
        $stmt->bindValue(':h', empty($data['h']) ? null : $data ['h']);
        $stmt->bindValue(':b', empty($data['b']) ? null : $data ['b']);
        $stmt->execute();
    } catch (Exception $ex) {
        if ($ex->getCode() == 23505) {
            $tplVars['form'] = $data;
            $tplVars['error'] = 'Tato osoba uz existuje.';
            return $this->view->render($response, 'formular.latte', $tplVars);
        } else {
            $this->logger->error($ex->getMessage());
            die($ex->getMessage());
        }

    }
    return $response->withheader('Location', $this->router->pathForn('osoby'));
})->setName('editPerson');

$app->get('/pridej', function (Request $request, Response $response, $args) {
    $tplVars['form'] = ['ln' => '', 'fn' => '', 'nn' => '', 'u'=>'', 'b'=>'', 'c'=>'', 'psc'=>'' ];
    return $this->view->render($response, 'add-personwithaddress.latte', $tplVars);
})->setName('formular');


$app->post('/pridej', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody(); // v podstat2 $_POST
    if (!empty($data['fn']) &&
        !empty($data['nn']) &&
        !empty($data['ln'])&&
        !empty($data['u'])&&
        !empty($data['b'])&&
        !empty($data['c'])&&
        !empty($data['psc'])) {

        try {
            $stmt = $this->db->prepare('INSERT INTO person(
last_name, first_name, nickname, gender, height, birthday, street, number, psc)
VALUES(:fn, :ln, :nn, :g), :h :b, :u, :c, :psc');
            $g = empty($data['g']) ? null : $data ['g'];
            $stmt->bindValue(':g', $g);
            $stmt->bindValue(':fn', $data['fn']);
            $stmt->bindValue(':ln', $data['ln']);
            $stmt->bindValue(':nn', $data['nn']);
            $stmt->bindValue(':h', $data['h']);
            $stmt->bindValue(':b', $data['b']);
            $stmt->bindValue(':u', $data['u']);
            $stmt->bindValue(':c', $data['c']);
            $stmt->bindValue(':psc', $data['psc']);
            $stmt->execute();
        } catch (Exception $ex) {
            if ($ex->getCode() == 23505) {
                $tplVars['form'] = $data;
                $tplVars['error'] = 'Tato osoba uz existuje.';
                return $this->view->render($response, 'add-personwithaddress.latte', $tplVars);
            } else {
                $this->logger->error($ex->getMessage());
                die($ex->getMessage());
            }

        }
        return $response->withHeader(
            'Location',
            $this->router->pathFor('osoby'));
    } else {
        $tplVars['form'] = $data;
        return $this->view->render($response, 'add-personwithaddress.latte', $tplVars);
    }
});


})->add(function (Request $request, Response $response, $next){
 if (!empty($_SESSION['user'])){
    return $next ($request, $response);
 }else {
     return $response->withHeader('Location', $this->router->pathFor('login'));
 }
});