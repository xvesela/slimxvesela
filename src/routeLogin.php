<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->post('/login',function(Request $request, Response $response, $args){
    $data =$request->getParsedBody();
    if(!empty($data['login'])&& !empty($data['pass'])){
        try {
            $stmt = $this->db -> prepare('SELECT * FROM account WHERE login = :l');
            $stmt->bindValue(':l', $data['login']);
            $stmt->execute();

            $user = $stmt->fetch();

            if(!empty($user)){
                if(password_verify($data['pass'], $user['password'])){
                    $_SESSION ['user'] = $user;
                    return $response->withHeader('Location', $this->router->pathFor('osoby'));
                }
            }
        } catch (Exception $e){
            $this->logger->error($e->getMessage());
            die ($e->getMessage());
        }

    }
    $tplVars['error']='Chyba přihlášeni';
    return $this->view->render(
        $response,
        "login.latte",
        $tplVars);
});

$app->get('/login',function(Request $request, Response $response, $args){
      return $this->view->render($response,'login.latte');
})->setName('login');

/**
 * Created by PhpStorm.
 * User: xvesela
 * Date: 12.12.2018
 * Time: 11:26
 */